package me.aberrantfox.diskordkt.gateway.data

class Discord {
    companion object {
        const val Api = "https://discordapp.com/api/"
        const val Version = 7
        const val Encoding = "json"
        const val WssQueryString = "?v=$Version&encoding=$Encoding"
    }
}

class Routes {
    companion object {
        const val Gateway = "${Discord.Api}/gateway"
        const val GatewayBot = "${Discord.Api}/bot"
    }
}

enum class OpCodes {
    Dispatch,
    Heartbeat,
    Identify,
    StatusUpdate,
    VoiceStatusUpdate,
    VoiceGuildPing,
    Resume,
    Reconnect,
    RequestGuildMembers,
    InvalidSession,
    Hello,
    HeartbeatAck;

    val code = ordinal
}

enum class VoiceOpCodes(val code: Int) {
    Identify(0),
    SelectProtocol(1),
    Ready(2),
    Heartbeat(3),
    SessionDescription(4),
    Speaking(5),
    Hello(8),
    ClientConnect(12),
    ClientDisconnect(13)
}