package me.aberrantfox.diskordkt.gateway.data

/**
 * Data classes in this package should have the exact name found on the API response,
 * for example, in Payload we use `v` and not `version`, since `v` is on the response object.
 */

data class Payload(val op: Int, val d: String, val s: Int, val t: String)

data class GatewayResponse(val url: String)

data class GatewayBotResponse(val url: String, val shards: Int)

data class GatewayHello(val heartbeat_interval: Int, val _trace: Array<String>)