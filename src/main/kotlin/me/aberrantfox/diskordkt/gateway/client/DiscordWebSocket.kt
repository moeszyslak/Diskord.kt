package me.aberrantfox.diskordkt.gateway.client


import mu.KLogging
import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ServerHandshake
import java.lang.Exception
import java.net.URI

class DiscordWebSocket(wsURL: String) : WebSocketClient(URI.create(wsURL)) {
    companion object : KLogging()

    override fun onOpen(handshake: ServerHandshake) {
        logger.debug { "Connection opened" }
    }

    override fun onClose(code: Int, reason: String, remote: Boolean) {
        logger.debug { "Socket Closed for $reason - code $code" }
    }

    override fun onMessage(message: String) {
        logger.debug { "Message received: $message" }
    }

    override fun onError(e: Exception) {
        logger.debug(e.message, e)
    }
}