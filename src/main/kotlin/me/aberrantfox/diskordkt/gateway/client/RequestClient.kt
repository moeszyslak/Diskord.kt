package me.aberrantfox.diskordkt.gateway.client

import com.github.kittinunf.fuel.httpGet
import com.google.gson.GsonBuilder
import me.aberrantfox.diskordkt.gateway.data.Discord
import me.aberrantfox.diskordkt.gateway.data.GatewayResponse
import me.aberrantfox.diskordkt.gateway.data.Routes
import mu.KLogging

class RequestClient {
    companion object : KLogging()

    private val wssUrl: String
    private val socket: DiscordWebSocket
    private val gson =  GsonBuilder().setPrettyPrinting().create()

    init {
        logger.debug { "Attempt to connect to gateway" }

        val (_, _, result) = Routes.Gateway.httpGet().responseString()
        val responseURL = gson.fromJson(result.get(), GatewayResponse::class.java).url
        wssUrl = "$responseURL${Discord.WssQueryString}"
        socket = DiscordWebSocket(wssUrl)
        socket.connect()

        logger.debug { "Websocket URL collected: $wssUrl" }
    }
}